# K4 Image Optimizer plugin for Craft CMS 3.x

Image Optimizer

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require k4/k4-image-optimizer

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for K4 Image Optimizer.

## K4 Image Optimizer Overview

1. In Control Panel open the settings page
2. Supply API-Keys for Tinify or ShortPixel
3. Supply binary path for your local image optimization tools like jpegoptim,optipng,pngquant,svgo,gifsicle

## Configuring K4 Image Optimizer

-Insert text here-

## Using K4 Image Optimizer

1. In Control Panel / Utilities choose "Bilder optimieren"
2. Choose your optimization Method
3. All images with a preceeding "_" will get optimized

## K4 Image Optimizer Roadmap

Some things to do, and ideas for potential features:

* add translations

Brought to you by [Christian Hiller](https://kreisvier.ch)
