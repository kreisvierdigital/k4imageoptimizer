<?php
/**
 * K4 Image Optimizer plugin for Craft CMS 3.x
 *
 * Image Optimizer
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Thomas Bauer
 */

namespace k4\k4imageoptimizer;

use craft\services\ProjectConfig;
use flipbox\craft\psr3\Logger;
use k4\k4imageoptimizer\models\Settings;
use k4\k4imageoptimizer\services\K4ImageOptimizerService;
use k4\k4imageoptimizer\utilities\K4ImageOptimizerUtility as K4ImageOptimizerUtilityUtility;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\UrlManager;
use craft\services\Utilities;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterUrlRulesEvent;
use craft\console\Application as ConsoleApplication;

use yii\base\Event;

/**
 * Class K4ImageOptimizer
 *
 * @author    Thomas Bauer
 * @package   K4ImageOptimizer
 * @since     1.0.0
 *
 * @property  K4ImageOptimizerService $data
 */
class K4ImageOptimizer extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var K4ImageOptimizer
     */
    public static $plugin;

    public bool $hasCpSettings = true;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public string $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        $this->initConsoleCommands();
        $this->initServices();
        $this->initUtilities();
        $this->initRoutes();
        $this->initLogging();
        $this->initEvents();

        Craft::info('event::k4-image-optimizer start');


    }

    // Protected Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    protected function createSettingsModel(): ?\craft\base\Model
    {
        return new Settings();
    }

    /**
     * @inheritdoc
     */
    protected function settingsHtml(): string
    {
        return Craft::$app->view->renderTemplate(
            'k4-image-optimizer/settings',
            [
                'settings' => $this->getSettings()
            ]
        );
    }


    protected function initUtilities(){
//        Event::on(
//            Utilities::class,
//            Utilities::EVENT_REGISTER_UTILITY_TYPES,
//            function (RegisterComponentTypesEvent $event) {
//                $event->types[] = K4ImageOptimizerUtilityUtility::class;
//            }
//        );
    }

    protected function initServices()
    {
        $this->setComponents(
            [
                'data'         => K4ImageOptimizerService::class,
                'logger'       => Logger::class
            ]
        );
    }

    private function initRoutes()
    {
        // Register our site routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $routes       = include __DIR__ . '/routes_site.php';
                $event->rules = array_merge($event->rules, $routes);
            }
        );


        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $routes       = include __DIR__ . '/routes_cp.php';
                $event->rules = array_merge($event->rules, $routes);
            }
        );
    }

    private function initLogging()
    {
        Craft::info(
            Craft::t(
                'k4-image-optimizer',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    private function initConsoleCommands(){
        // Add in our console commands
        if (Craft::$app instanceof ConsoleApplication) {
            $this->controllerNamespace = 'k4\k4imageoptimizer\console\controllers';
        }
    }

    private function initEvents(){
        // Do something after we're installed
        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {


                    $settings = new Settings();
                    $settings->optimizeTool = 'local';
                    $settings->apiKey = md5(time());

                    $plugins = Craft::$app->getPlugins();
                    Craft::$app->getProjectConfig()->set(ProjectConfig::PATH_PLUGINS . '.' . $this->handle . '.settings', $settings->toArray());

                    Craft::info('event::k4-image-optimizer installed');
                }
            }
        );

        // Do something after we're installed
        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_ENABLE_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                    Craft::info('event::k4-image-optimizer enabled');
                }
            }
        );

        // Do something after we're installed
        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_UNINSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                    Craft::info('event::k4-image-optimizer uninstalled');
                }
            }
        );
    }


}
