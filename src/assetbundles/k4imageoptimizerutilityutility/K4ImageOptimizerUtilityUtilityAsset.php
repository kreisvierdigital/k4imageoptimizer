<?php
/**
 * K4 Image Optimizer plugin for Craft CMS 3.x
 *
 * Image Optimizer
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Thomas Bauer
 */

namespace k4\k4imageoptimizer\assetbundles\k4imageoptimizerutilityutility;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * @author    Thomas Bauer
 * @package   K4ImageOptimizer
 * @since     1.0.0
 */
class K4ImageOptimizerUtilityUtilityAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = "@k4/k4imageoptimizer/assetbundles/k4imageoptimizerutilityutility/dist";

        $this->depends = [
            CpAsset::class,
        ];

        $this->js = [
            'js/K4ImageOptimizerUtility.js',
        ];

        $this->css = [
            'css/K4ImageOptimizerUtility.css',
        ];

        parent::init();
    }
}
