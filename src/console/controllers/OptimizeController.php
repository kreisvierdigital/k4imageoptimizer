<?php
/**
 * K4 Image Optimizer plugin for Craft CMS 3.x
 *
 * test
 *
 * @link      kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

namespace k4\k4imageoptimizer\console\controllers;

use k4\k4imageoptimizer\K4ImageOptimizer;

use Craft;
use k4\k4imageoptimizer\models\Settings;
use k4\k4imageoptimizer\services\K4ImageOptimizerService;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ShortPixel\ShortPixel;
use Tinify\Tinify;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

/**
 * Default Command
 *
 * The first line of this class docblock is displayed as the description
 * of the Console Command in ./craft help
 *
 * Craft can be invoked via commandline console by using the `./craft` command
 * from the project root.
 *
 * Console Commands are just controllers that are invoked to handle console
 * actions. The segment routing is plugin-name/controller-name/action-name
 *
 * The actionIndex() method is what is executed if no sub-commands are supplied, e.g.:
 *
 * ./craft k4-image-optimizer/default
 *
 * Actions must be in 'kebab-case' so actionDoSomething() maps to 'do-something',
 * and would be invoked via:
 *
 * ./craft k4-image-optimizer/default/do-something
 *
 * @author    Christian Hiller
 * @package   K4ImageOptimizer
 * @since     1.0.0
 */
class OptimizeController extends Controller
{
    // Public Methods
    // =========================================================================

    /**
     * Handle k4-image-optimizer/default console commands
     *
     * The first line of this method docblock is displayed as the description
     * of the Console Command in ./craft help
     *
     * @return mixed
     */
    public function actionRun()
    {
        $count = [];

        /**
         * @var $data Settings
         */
        $settings = K4ImageOptimizer::$plugin->getSettings();

        /**
         * @var $data K4ImageOptimizerService
         */
        $data = K4ImageOptimizer::getInstance()->data;

        set_time_limit(300);

        $optimizeTool = $settings->optimizeTool;

        if (empty($optimizeTool)) $optimizeTool = "local"; //tinypng, shortpixel or local

        //TinyPNG Config
        Tinify::setKey(Craft::parseEnv($settings->tinifyApiKey));

        //ShortPixel Config
        ShortPixel::setKey(Craft::parseEnv($settings->shortPixelApiKey));
        ShortPixel::setOptions(array("lossy" => 1)); // Compression level: 0 - lossless, 1 - lossy (default), 2 - glossy

        $files = [];

        $di = new RecursiveDirectoryIterator(Craft::getAlias($settings->baseFolder));
        foreach (new RecursiveIteratorIterator($di) as $filename => $file) {

            if ($data->validateFilename($filename)) {
                $file = realpath($filename);
                if ($optimizeTool == "tinypng"){
                    $data->tinyPNGCompress($file);
                }
                elseif ($optimizeTool == "shortpixel"){
                    $data->shortpixel($file);
                }
                else{
                    $data->localOptimizer($file);
                }
                $this->stdout($file.PHP_EOL, Console::FG_GREEN);
                $files[] = $file;
            }
        }

        $this->stdout(count($files)." files".PHP_EOL, Console::FG_GREEN);
        return ExitCode::OK;
    }

    public function actionResetFilesDates(){
        $count = [];

        /**
         * @var $data Settings
         */
        $settings = K4ImageOptimizer::$plugin->getSettings();

        /**
         * @var $data K4ImageOptimizerService
         */
        $data = K4ImageOptimizer::getInstance()->data;

        $files = [];

        $di = new RecursiveDirectoryIterator(Craft::getAlias($settings->baseFolder));
        foreach (new RecursiveIteratorIterator($di) as $filename => $file) {
            if ($data->validateFilename($filename)) {
                $time = time();
                touch($file, $time);

                $this->stdout($file.PHP_EOL, Console::FG_GREEN);
                $files[] = $file;

            }
        }
        $this->stdout(count($files)." files".PHP_EOL, Console::FG_GREEN);
        return ExitCode::OK;
    }

    public function actionMarkDone(){
        $count = [];

        /**
         * @var $data Settings
         */
        $settings = K4ImageOptimizer::$plugin->getSettings();

        /**
         * @var $data K4ImageOptimizerService
         */
        $data = K4ImageOptimizer::getInstance()->data;

        $files = [];

        $di = new RecursiveDirectoryIterator(Craft::getAlias($settings->baseFolder));
        foreach (new RecursiveIteratorIterator($di) as $filename => $file) {
            if ($data->validateFilename($filename)) {

                $time = time() + 300000000; // ~8 Jahre
                touch($file, $time);

                $this->stdout($file.PHP_EOL, Console::FG_GREEN);
                $files[] = $file;

            }
        }
        $this->stdout(count($files)." files".PHP_EOL, Console::FG_GREEN);
        return ExitCode::OK;

    }

}
