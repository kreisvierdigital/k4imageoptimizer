<?php
/**
 * K4 Image Optimizer plugin for Craft CMS 3.x
 *
 * Image Optimizer
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Thomas Bauer
 */

namespace k4\k4imageoptimizer\controllers;

use flipbox\craft\psr3\Logger;
use k4\k4imageoptimizer\K4ImageOptimizer;

use Craft;
use craft\web\Controller;
use k4\k4imageoptimizer\models\Settings;
use k4\k4imageoptimizer\services\K4ImageOptimizerService;
use Kint\Kint;

/**
 * @author    Thomas Bauer
 * @package   K4ImageOptimizer
 * @since     1.0.0
 */
class DefaultController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected array|int|bool $allowAnonymous = [];

    // Public Methods
    // =========================================================================

    /**
     * @return mixed
     */
    public function actionOptimize()
    {
        $request = Craft::$app->request;

        if ($request->isPost)
        {
            //$optimizeTool = $request->getBodyParam('optimizeTool');
            $apiKey = $request->getBodyParam('apiKey');

        }
        else if ($request->isGet)
        {
            //$optimizeTool = $request->getQueryParam('optimizeTool');
            $apiKey = $request->getQueryParam('apiKey');

        }

        $settings = K4ImageOptimizer::getInstance()->getSettings();
        $optimizeTool = $settings->optimizeTool;


        if (!$this->validateApiKey($apiKey)) return $this->asJson('wrong ApiKey');

        if (!$this->validateOptimizeTool($optimizeTool)) return $this->asJson('wrong optimizeTool');

        return $this->optimize($optimizeTool);

    }

    private function validateApiKey($apiKey)
    {
        /**
         * @var $settings Settings
         */
        $settings = K4ImageOptimizer::getInstance()->getSettings();

        if (Craft::parseEnv($settings->apiKey) == $apiKey){
            return true;
        }

        return false;
    }

    private function validateOptimizeTool($optimizeTool)
    {
        $tools = ['local','tinypng','shortpixel'];

        if (in_array($optimizeTool,$tools)){
            return true;
        }

        return false;
    }

    private function optimize($optimizeTool){

        /**
         * @var $data K4ImageOptimizerService
         */
        $data = K4ImageOptimizer::getInstance()->data;

        /**
         * @var $logger Logger
         */
        $logger = K4ImageOptimizer::getInstance()->logger;

        $files = $data->optimize($optimizeTool,$logger);

        return $this->asJson($files);


    }

}
