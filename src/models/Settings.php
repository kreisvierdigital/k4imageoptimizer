<?php
/**
 * test plugin for Craft CMS 3.x
 *
 * test
 *
 * @link      kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

namespace k4\k4imageoptimizer\models;

use Craft;
use craft\base\Model;
use Kint\Kint;

/**
 * Test Settings Model
 *
 * This is a model used to define the plugin's settings.
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @property  string $optimizeTool
 *
 * @author    Christian Hiller
 * @package   Test
 * @since     1.0.0
 */
class Settings extends Model
{
    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $optimizeTool;

    /**
     * @var string
     */
    public $tinifyApiKey;

    /**
     * @var string
     */
    public $shortPixelApiKey;

    /**
     * @var string
     */
    public $binaryPath;

    /**
     * @var string
     */
    public $apiKey;

    /**
     * @var string
     */
    public $baseFolder;
    /**
     * @var string
     */
    public $pluginBinaries;

    /**
     * @var string
     */
    public $excludedStrings;

    /**
     * @return Settings
     */
    public function init(): void
    {
        parent::init();
    }

    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            [['tinifyApiKey', 'shortPixelApiKey','baseFolder','excludedStrings'],'string'],
        ];
    }
}
