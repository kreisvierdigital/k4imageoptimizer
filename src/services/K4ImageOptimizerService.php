<?php
/**
 * K4 Image Optimizer plugin for Craft CMS 3.x
 *
 * Image Optimizer
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Thomas Bauer
 */

namespace k4\k4imageoptimizer\services;

use flipbox\craft\psr3\Logger;
use k4\k4imageoptimizer\K4ImageOptimizer;

use Craft;
use craft\base\Component;
use k4\k4imageoptimizer\models\Settings;
use putyourlightson\logtofile\LogToFile;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ShortPixel\Commander;
use ShortPixel\ShortPixel;
use ShortPixel\Source;
use Spatie\ImageOptimizer\OptimizerChain;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use Spatie\ImageOptimizer\Optimizers\BaseOptimizer;
use Spatie\ImageOptimizer\Optimizers\Gifsicle;
use Spatie\ImageOptimizer\Optimizers\Jpegoptim;
use Spatie\ImageOptimizer\Optimizers\Optipng;
use Spatie\ImageOptimizer\Optimizers\Pngquant;
use Spatie\ImageOptimizer\Optimizers\Svgo;
use Tinify\Tinify;

/**
 * @author    Thomas Bauer
 * @package   K4ImageOptimizer
 * @since     1.0.0
 */
class K4ImageOptimizerService extends Component
{
    // Public Methods
    // =========================================================================

    public $files = [];

    /**
     * @var Logger
     */
    public $logger;

    /**
     * @var Settings
     */
    public $settings;

    /*
     * @return mixed
     */
    public function optimize($optimizeTool, $logger, $console = false)
    {
        $this->logger = $logger;
        $this->settings = K4ImageOptimizer::$plugin->getSettings();

        //set_time_limit(300);

        if (empty($optimizeTool)) {
            $optimizeTool = "local";
        } //tinypng, shortpixel or local

        //TinyPNG Config
        Tinify::setKey(Craft::parseEnv($this->settings->tinifyApiKey));

        //ShortPixel Config
        ShortPixel::setKey(Craft::parseEnv($this->settings->shortPixelApiKey));
        ShortPixel::setOptions(array("lossy" => 1)); // Compression level: 0 - lossless, 1 - lossy (default), 2 - glossy

        $files = [];

        $di = new RecursiveDirectoryIterator(Craft::getAlias($this->settings->baseFolder));
        foreach (new RecursiveIteratorIterator($di) as $filename => $file) {
            if ($this->validateFilename($filename)) {
                $file = realpath($filename);
                if ($optimizeTool == "tinypng") {
                    $this->tinyPNGCompress($file);
                } elseif ($optimizeTool == "shortpixel") {
                    $this->shortpixel($file);
                } else {
                    $this->localOptimizer($file);
                }
            }
        }

        return $this->files;

    }

    public function validateFilename($filename) {

        $this->settings = K4ImageOptimizer::$plugin->getSettings();

        if (
            substr(strtolower($filename), -4) !== '.png' &&
            substr(strtolower($filename), -4) !== '.jpg' &&
            substr(strtolower($filename), -5) !== '.jpeg')
        {
            return false;
        }

        if (strpos($filename, '/_') === false)
        {
            return false;
        }

        if (!empty($this->settings->excludedStrings) &&
            json_decode($this->settings->excludedStrings) !== null &&
            is_array(json_decode($this->settings->excludedStrings)) )
        {
            forEach(json_decode($this->settings->excludedStrings) as $fileNamePartialString)
            {
                if (strpos($filename, $fileNamePartialString) !== false)
                {
                    //LogToFile::log($filename,'k4-image-optimizer');
                    return false;
                }
            }
        }

        return true;


    }

    public function localOptimizer($file)
    {

        if (date(filemtime($file)) < time()) {

            if ($this->settings->pluginBinaries == true) {
                $test= Craft::getAlias('@vendor') . "/k4/k4-image-optimizer/bin";
                $optimizerChain = $this->getLocalOptimizerChain(Craft::getAlias('@vendor') . "/k4/k4-image-optimizer/bin");
            } else {
                if (!empty($this->settings->binaryPath)) {
                    $optimizerChain = $this->getLocalOptimizerChain($this->settings->binaryPath);
                } else {
                    $optimizerChain = OptimizerChainFactory::create();
                }
            }

            $optimizerChain->useLogger($this->logger);

            $optimizerChain->optimize($file);
            //optimized file overwrites original one

            $time = time() + 300000000; // ~8 Jahre

            // Ändern der Datei
            if (!touch($file, $time)) {
                //echo 'Ein Fehler ist aufgetreten ...';
            } else {
                //echo 'Änderung der Modifikationszeit war erfolgreich';
            }

            $this->files[] = "Bild optimiert mit localOptimizer: " . $file;

        } else {
            $this->files[] = "Bild bereits optimiert: " . $file;
        }


    }

    public function shortpixel($file)
    {
        if (date(filemtime($file)) < time()) {

            $ret = \ShortPixel\fromFile($file)->optimize(1)->toFiles(dirname($file), basename($file));

            $time = time() + 300000000; // ~8 Jahre

            // Ändern der Datei
            if (!touch($file, $time)) {
                //echo 'Ein Fehler ist aufgetreten ...';
            } else {
                //echo 'Änderung der Modifikationszeit war erfolgreich';
            }

            $this->files[] = "Bild optimiert mit Shortpixel: " . $file;

        } else {
            $this->files[] = "Bild bereits optimiert: " . $file;

        }


    }

    public function tinyPNGCompress($file)
    {
        if (date(filemtime($file)) < time()) {

            $source = \Tinify\fromFile($file);
            $source->toFile($file);
            $time = time() + 300000000; //~ 8 Jahre

            // Ändern der Datei
            if (!touch($file, $time)) {
                //echo 'Ein Fehler ist aufgetreten ...';
            } else {
                //echo 'Änderung der Modifikationszeit war erfolgreich';
            }

            $this->files[] = "Bild optimiert mit TinyPNG: " . $file;

        } else {

            $this->files[] = "Bild bereits optimiert: " . $file;

        }


    }

    private function getLocalOptimizerChain($binaryPath)
    {

        $jpegOptim = new Jpegoptim([
            '-m85',
            '--strip-all',
            '--all-progressive',
        ]);
        $jpegOptim->setBinaryPath($binaryPath);

        $optiPng = new Optipng([
            '-i0',
            '-o2',
            '-quiet',
        ]);
        $optiPng->setBinaryPath($binaryPath);

        $pngQuant = new Pngquant([
            '--force',
        ]);
        $pngQuant->setBinaryPath($binaryPath);

        $svgO = new Svgo([
            '--disable=cleanupIDs',
        ]);
        $svgO->setBinaryPath($binaryPath);

        $gifSicle = new Gifsicle([
            '-b',
            '-O3',
        ]);
        $gifSicle->setBinaryPath($binaryPath);

        return (new OptimizerChain())
            ->addOptimizer($jpegOptim)
            ->addOptimizer($optiPng)
            ->addOptimizer($pngQuant)
            ->addOptimizer($svgO)
            ->addOptimizer($gifSicle);

    }

}
