<?php
/**
 * K4 Image Optimizer plugin for Craft CMS 3.x
 *
 * Image Optimizer
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Thomas Bauer
 */

/**
 * @author    Thomas Bauer
 * @package   K4ImageOptimizer
 * @since     1.0.0
 */
return [
    'K4 Image Optimizer plugin geladen' => 'K4 Image Optimizer plugin loaded',
    'Bilder optimieren' => 'Optimize Images',
    'Bilder im @webroot Ordner mit "_" am Anfang im Dateinamen werden optimiert' => 'Images in @webroot folder with trailing "_" get optimized.',
    'Welches Tool möchten Sie benutzten?' => 'Which tools do you like to use?',
    'Tinify API-Schluessel' => 'Tinify API Key',
    'ShortPixel API-Schluessel' => 'ShortPixel API Key',
    'Pfad zu den lokalen Tools' => 'Path to local binaries',
    'Api-Schluessel' => 'Api Key',
    'Cron Job' => 'Cron Job',
    'Ihr API-Schluessel zur Benutzung z.B. fuer die automatisierte Bildoptimierung in cron jobs' => 'Your API Key for usage in e.g. automated image optimization in cron jobs',
    'Erstellen Sie einen "cron job" mit der folgenden Zeile um einen automatisierten Prozess zu starten' => 'Create a cron job with the following syntax to run an automated process',
    'Sie koennen einen Pfad angeben fuer ihre lokalen Optimierungstools wie z.B. pnqquant,optipng' => 'Provide a path to your local binaries e.g. pnqquant,optipng',
    'Local, TinyPNG oder ShortPixel' => 'Local, TinyPNG or ShortPixel',
    'benutze die statischen "Binaries" fuer Mac OS welche im Plugin mitgeliefert werden' => 'use static binaries for Mac Os provided by this plugin',
    'ausgeschlossene Strings in Dateinamen' => 'excluded strings in filenames',
    'Bitte geben sie die Strings als JSON array an.'=>'Please provide the strings in JSON array format.'

];
