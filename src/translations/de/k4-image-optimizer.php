<?php
/**
 * K4 Image Optimizer plugin for Craft CMS 3.x
 *
 * Image Optimizer
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Thomas Bauer
 */

/**
 * @author    Thomas Bauer
 * @package   K4ImageOptimizer
 * @since     1.0.0
 */
return [
    'K4 Image Optimizer plugin geladen' => 'K4 Image Optimizer plugin geladen',
    'Bilder optimieren' => 'Bilder optimieren',
    'Bilder im @webroot Ordner mit "_" am Anfang im Dateinamen werden optimiert' => 'Bilder im @webroot Ordner mit "_" am Anfang im Dateinamen werden optimiert',
    'Welches Tool möchten Sie benutzten?' => 'Welches Tool möchten Sie benutzten?',
    'Tinify API-Schluessel' => 'Tinify API-Schluessel',
    'ShortPixel API-Schluessel' => 'ShortPixel API-Schluessel',
    'Pfad zu den lokalen Tools' => 'Pfad zu den lokalen Tools',
    'Api-Schluessel' => 'Api-Schluessel',
    'Cron Job' => 'Cron Job',
    'Ihr API-Schluessel zur Benutzung z.B. fuer die automatisierte Bildoptimierung in cron jobs' => 'Ihr API-Schluessel zur Benutzung z.B. fuer die automatisierte Bildoptimierung in cron jobs',
    'Erstellen Sie einen "cron job" mit der folgenden Zeile um einen automatisierten Prozess zu starten' => 'Erstellen Sie einen "cron job" mit der folgenden Zeile um einen automatisierten Prozess zu starten',
    'Sie koennen einen Pfad angeben fuer ihre lokalen Optimierungstools wie z.B. pnqquant,optipng' => 'Sie koennen einen Pfad angeben fuer ihre lokalen Optimierungstools wie z.B. pnqquant,optipng',
    'Local, TinyPNG oder ShortPixel' => 'Local, TinyPNG oder ShortPixel',
    'benutze die statischen "Binaries" fuer Mac OS welche im Plugin mitgeliefert werden' => 'benutze die statischen "Binaries" fuer Mac OS welche im Plugin mitgeliefert werden',
    'ausgeschlossene Strings in Dateinamen' => 'ausgeschlossene Strings in Dateinamen',
    'Bitte geben sie die Strings als JSON array an.'=>'Bitte geben sie die Strings als JSON array an.'
];
