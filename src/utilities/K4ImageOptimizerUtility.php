<?php
/**
 * K4 Image Optimizer plugin for Craft CMS 3.x
 *
 * Image Optimizer
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Thomas Bauer
 */

namespace k4\k4imageoptimizer\utilities;

use k4\k4imageoptimizer\K4ImageOptimizer;
use k4\k4imageoptimizer\assetbundles\k4imageoptimizerutilityutility\K4ImageOptimizerUtilityUtilityAsset;

use Craft;
use craft\base\Utility;
use k4\k4imageoptimizer\services\K4ImageOptimizerService;

/**
 * K4 Image Optimizer Utility
 *
 * @author    Thomas Bauer
 * @package   K4ImageOptimizer
 * @since     1.0.0
 */
class K4ImageOptimizerUtility extends Utility
{
    // Static
    // =========================================================================

    /**
     * @inheritdoc
     */
    public static function displayName(): string
    {
        return Craft::t('k4-image-optimizer', 'Bilder optimieren');
    }

    /**
     * @inheritdoc
     */
    public static function id(): string
    {
        return 'k4imageoptimizer-k4-image-optimizer-utility';
    }

    /**
     * @inheritdoc
     */
    public static function iconPath(): ?string
    {
        return Craft::getAlias("@k4/k4imageoptimizer/assetbundles/k4imageoptimizerutilityutility/dist/img/K4ImageOptimizerUtility-icon.svg");
    }

    /**
     * @inheritdoc
     */
    public static function badgeCount(): int
    {
        return 0;
    }

    /**
     * @inheritdoc
     */
    public static function contentHtml(): string
    {
        Craft::$app->getView()->registerAssetBundle(K4ImageOptimizerUtilityUtilityAsset::class);
        Craft::$app->getView()->registerJs('new Craft.OptimizeImagesUtility(\'optimize-images\');');

        // was submitted ?
        if (Craft::$app->request->isPost) {

            /**
             * @var $data K4ImageOptimizerService
             */
            $data = K4ImageOptimizer::getInstance()->data;

            $data->optimize();

            $result = 'Optimiert';
        }

        return Craft::$app->getView()->renderTemplate(
            'k4-image-optimizer/_components/utilities/K4ImageOptimizerUtility_content', []);
    }
}
